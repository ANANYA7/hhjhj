package net.guides.springboot2.jdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import net.guides.springboot2.jdbc.model.Employee;
import net.guides.springboot2.jdbc.repository.EmployeeMyBatisRepository;

@RestController
public class Controller {

	@Autowired
	EmployeeMyBatisRepository repository;
	
	@GetMapping("/api")
	public List<Employee> getAllEmployees() {
		
		
		
		return repository.findAll();
	}
	
	
}
